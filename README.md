This repository contains an example Unity project that shows a basic architecture embracing Unity's component based system.
* No manager classes/objects.
* No inheritance between components.
* No interfaces on components.
* Loose coupling.
* Use of the UnityEvent class.
 
Note: Using interfaces or inheritance on components isn't necessarily bad; however, I recommend to always consider twice before using them.
Either way, this example project shows that simple contexts don't require these things at all.

The project contains a *very* rudimentary implementation of Ryan Hipple's ScriptableObject architecture,
as presented in his [GDC talk](https://www.youtube.com/watch?v=raQ3iHhE_Kk).
This code allows the project to showcase more best practices:
* Proper use of Unity-Editor-style DI in many situations.
* No singletons.