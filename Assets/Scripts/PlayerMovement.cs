﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The class that transfers input to movement for the player entity.
/// Check AIFollowMovement.cs for more info.
/// </summary>
[RequireComponent(typeof(EntityMovement))]
public class PlayerMovement : MonoBehaviour
{
    private EntityMovement entityMovement;

    private void Awake()
    {
        entityMovement = GetComponent<EntityMovement>();
    }

    private void FixedUpdate()
    {
        var input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        entityMovement.Move(input);
    }
}
