﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using MiniHipple;

/// <summary>
/// A simple health class showcasing reusability and small components.
/// Instead of having PlayerHealth and EnemyHealth, we have one health component class
/// that both players and enemies can use.
/// The onDeath event can be used to define behaviour without having to change code.
/// </summary>
public class Health : MonoBehaviour
{
    [SerializeField]
    private FloatReference _health;
    [SerializeField]
    private FloatReference maxHealth;
    [Space]
    [SerializeField]
    private UnityEvent onDeath;

    public float health
    {
        get { return _health.value; }
    }

    public void DecreaseHealth(float amount)
    {
        if (amount <= 0) return;

        if (_health.value > amount)
        {
            _health.value -= amount;
        }
        else
        {
            _health.value = 0;
            onDeath.Invoke();
            Destroy(gameObject);
        }
    }

    public void IncreaseHealth(float amount)
    {
        if (amount <= 0) return;

        var targetHealth = _health.value + amount;
        if (targetHealth >= maxHealth.value)
        {
            _health.value = maxHealth.value;
        }
        else
        {
            _health.value = targetHealth;
        }
    }
}
