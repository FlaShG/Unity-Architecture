﻿using UnityEngine;
using MiniHipple;

/// <summary>
/// This class demonstrates how to interact with other objects
/// without using messages, tags or other kinds of strings.
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class DamageTrigger : MonoBehaviour
{
    [SerializeField]
    private FloatReference damagePerSecond;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.isTrigger) return;

        // Instead of checking whether the triggering object fulfills some special criteria,
        // like having a specific tag, we just check if the component of interest is there.
        // Since the health component is a small, reusable component (and we don't have PlayerHealth and EnemyHealth),
        // we can just work with it like with an interface in an OOP approach.
        var health = other.GetComponentInParent<Health>();
        if (health)
        {
            health.DecreaseHealth(damagePerSecond.value * Time.deltaTime);
        }
    }
}
