﻿using UnityEngine;
using MiniHipple;

public class GameObjectValueSignup : MonoBehaviour
{
    [SerializeField]
    private GameObjectValue target;

    private void OnEnable()
    {
        target.value = gameObject;
        target.AddMonitor(OnUpdateTarget);
    }

    private void OnDisable()
    {
        target.RemoveMonitor(OnUpdateTarget);
        if (target.value == gameObject)
        {
            target.value = null;
        }
    }

    private void OnUpdateTarget(GameObject newValue)
    {
        enabled = false;
    }
}
