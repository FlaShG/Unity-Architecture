﻿using UnityEngine;
using System.Collections;
using MiniHipple;

/// <summary>
/// A simple movement class showcasing reusability and small components.
/// The class can be reused for players and enemies alike.
/// The RequireComponent attribute is not to be underestimated for good component-based architecture,
/// as it fulfills a similar role to defining a superclass in an object-oriented approach.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class EntityMovement : MonoBehaviour
{
    new private Rigidbody2D rigidbody2D;
    [SerializeField]
    private FloatReference speed;

    /// <summary>
    /// Always use Awake for getting components that this object will use until the end of its lifetime.
    /// </summary>
    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector2 direction)
    {
        rigidbody2D.velocity = direction.normalized * speed.value;
    }
}
