﻿using UnityEngine;
using UnityEngine.UI;
using MiniHipple;

[RequireComponent(typeof(Text))]
public class IntValueUIDisplay : MonoBehaviour
{
    private Text text;
    [SerializeField]
    private IntReference target;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        target.AddMonitor(OnUpdateTarget);
        OnUpdateTarget(target.value);
    }

    private void OnDisable()
    {
        target.RemoveMonitor(OnUpdateTarget);
    }

    private void OnUpdateTarget(int value)
    {
        text.text = value + "";
    }
}
