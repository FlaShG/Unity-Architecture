﻿using UnityEngine;
using UnityEngine.UI;
using MiniHipple;

[RequireComponent(typeof(Text))]
public class FloatValueUIDisplay : MonoBehaviour
{
    private Text text;
    [SerializeField]
    private FloatReference target;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        target.AddMonitor(OnUpdateTarget);
        OnUpdateTarget(target.value);
    }

    private void OnDisable()
    {
        target.RemoveMonitor(OnUpdateTarget);
    }

    private void OnUpdateTarget(float value)
    {
        text.text = Mathf.FloorToInt(value) + "";
    }
}
