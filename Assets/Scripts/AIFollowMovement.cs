﻿using UnityEngine;
using MiniHipple;

/// <summary>
/// The class that transfers input to movement for the player entity.
/// Notice the use of multiple RequireComponent attributes.
/// Building a similar structure in an object-oriented approach is likely to require multiple inheritance.
/// </summary>
[RequireComponent(typeof(EntityMovement))]
[RequireComponent(typeof(Health))]
public class AIFollowMovement : MonoBehaviour
{
    private EntityMovement entityMovement;
    private Health health;
    [SerializeField]
    private GameObjectReference target;
    [SerializeField]
    private IntReference followThreshold;

    private void Awake()
    {
        entityMovement = GetComponent<EntityMovement>();
        health = GetComponent<Health>();
    }

    private void FixedUpdate()
    {
        if (!target.value) return;

        var direction = target.value.transform.position - transform.position;

        if (health.health < followThreshold.value)
        {
            direction *= -1;
        }

        entityMovement.Move(direction);
    }
}
