﻿using UnityEngine;
using MiniHipple;

public class SimpleRotate2D : MonoBehaviour
{
    [SerializeField]
    private FloatReference rotation;


    private void FixedUpdate()
    {
        transform.Rotate(Vector3.forward * rotation.value * Time.deltaTime);
    }
}
