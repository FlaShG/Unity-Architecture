﻿using UnityEngine;
using MiniHipple;

[RequireComponent(typeof(Health))]
public class HealthRegeneration : MonoBehaviour
{
    private Health health;
    [SerializeField]
    private FloatReference healPerSecond;

    private void Awake()
    {
        health = GetComponent<Health>();
    }

    private void FixedUpdate()
    {
        health.IncreaseHealth(healPerSecond.value * Time.deltaTime);
    }
}
