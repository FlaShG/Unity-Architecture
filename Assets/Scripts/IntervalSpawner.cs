﻿using UnityEngine;
using MiniHipple;

public class IntervalSpawner : MonoBehaviour
{
    public GameObject prefab;
    [SerializeField]
    private FloatReference interval;
    private float elapsedTime = 0f;

    private void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime > interval.value)
        {
            elapsedTime -= interval.value;
            Instantiate(prefab, transform.position, Quaternion.identity);
        }
    }
}
