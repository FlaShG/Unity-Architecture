﻿
namespace MiniHipple
{
    using UnityEngine;
    using UnityEngine.Events;

    public class GameEventListener : MonoBehaviour
    {
        [SerializeField]
        private GameEvent target;
        [SerializeField]
        private UnityEvent response;

        private void OnEnable()
        {
            target.AddListener(this);
        }

        private void OnDisable()
        {
            target.RemoveListener(this);
        }

        internal void Notify()
        {
            response.Invoke();
        }
    }
}
