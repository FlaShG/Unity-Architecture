﻿
namespace MiniHipple
{
    using UnityEngine;
    using System.Collections.Generic;

    [CreateAssetMenu(menuName = "MiniHipple/GameEvent")]
    public class GameEvent : ScriptableObject
    {
        private List<GameEventListener> listeners = new List<GameEventListener>();

        internal void AddListener(GameEventListener listener)
        {
            listeners.Add(listener);
        }

        internal void RemoveListener(GameEventListener listener)
        {
            listeners.Remove(listener);
        }

        public void Raise()
        {
            for (var i = listeners.Count - 1; i >= 0; i--)
            {
                listeners[i].Notify();
            }
        }
    }
}
