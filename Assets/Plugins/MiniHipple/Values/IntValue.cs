﻿
namespace MiniHipple
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "MiniHipple/IntValue")]
    public class IntValue : ValueObject<int>
    {
        public void IncrementValue(int amount)
        {
            value += amount;
        }
    }
}
