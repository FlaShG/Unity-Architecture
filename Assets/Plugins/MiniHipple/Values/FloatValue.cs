﻿
namespace MiniHipple
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "MiniHipple/FloatValue")]
    public class FloatValue : ValueObject<float>
    {

    }
}
