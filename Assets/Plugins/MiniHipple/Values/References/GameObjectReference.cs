﻿
namespace MiniHipple
{
    using UnityEngine;

    [System.Serializable]
    public class GameObjectReference : ValueReference<GameObject, GameObjectValue>
    {

    }
}
