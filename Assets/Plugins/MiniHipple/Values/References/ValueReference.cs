﻿
namespace MiniHipple
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public abstract class ValueReference<T, VT> : ValueReferenceBase where VT : ValueObject<T>
    {
        [SerializeField]
        private VT valueObject;
        [SerializeField]
        private T localValue;
        [SerializeField]
        private bool useLocal = false;

        public T value
        {
            set
            {
                if (useLocal)
                {
                    if (!object.Equals(localValue, value))
                    {
                        localValue = value;
                        NotifyMonitors(value);
                    }
                }
                else
                {
                    if (!object.Equals(valueObject.value, value))
                    {
                        valueObject.value = value;
                        NotifyMonitors(value);
                    }
                }
            }
            get
            {
                if (useLocal)
                {
                    return localValue;
                }
                else
                {
                    return valueObject ? valueObject.value : default(T);
                }
            }
        }

        private void UpdateValueObjectSubscription()
        {
            if (!useLocal && valueObject)
            {
                if (monitors.Count == 1)
                {
                    valueObject.AddMonitor(NotifyMonitors);
                }
                else if (monitors.Count == 0)
                {
                    valueObject.RemoveMonitor(NotifyMonitors);
                }
            }
        }

        #region Monitors
        private List<Action<T>> monitors = new List<Action<T>>();

        public void AddMonitor(Action<T> monitor)
        {
            monitors.Add(monitor);
            UpdateValueObjectSubscription();
        }

        public void RemoveMonitor(Action<T> monitor)
        {
            monitors.Remove(monitor);
            UpdateValueObjectSubscription();
        }

        private void NotifyMonitors(T value)
        {
            for (var i = monitors.Count - 1; i >= 0; i--)
            {
                monitors[i](value);
            }
        }
        #endregion
    }
}
