﻿
namespace MiniHipple
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "MiniHipple/GameObjectValue")]
    public class GameObjectValue : ValueObject<GameObject>
    {

    }
}
