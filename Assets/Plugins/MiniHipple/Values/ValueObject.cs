﻿
namespace MiniHipple
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public abstract class ValueObject<T> : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private T _value;
        private T _runtimeValue;
        public T value
        {
            get { return _runtimeValue; }
            set
            {
                if (!object.Equals(_runtimeValue, value))
                {
                    _runtimeValue = value;
                    NotifyMonitors();
                }
            }
        }

        #region Monitors
        private List<Action<T>> monitors = new List<Action<T>>();

        public void AddMonitor(Action<T> monitor)
        {
            monitors.Add(monitor);
        }

        public void RemoveMonitor(Action<T> monitor)
        {
            monitors.Remove(monitor);
        }

        private void NotifyMonitors()
        {
            for (var i = monitors.Count - 1; i >= 0; i--)
            {
                monitors[i](value);
            }
        }
        #endregion

        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {
            _runtimeValue = _value;
        }
    }
}