﻿
namespace MiniHipple
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "MiniHipple/BoolValue")]
    public class BoolValue : ValueObject<bool>
    {

    }
}
